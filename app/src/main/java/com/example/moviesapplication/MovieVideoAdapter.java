package com.example.moviesapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.moviesapplication.dto.MovieResponseItem;

import java.util.ArrayList;

public class MovieVideoAdapter extends RecyclerView.Adapter<MovieVideoAdapter.ViewHolder> {
    ArrayList<String> movieVideoKeys;
    ArrayList<String> movieVideoNames;
    LayoutInflater mInflater;
    MovieVideoAdapter.ItemClickListener mClickListener;
    Context context;

    public MovieVideoAdapter(Context context,  ArrayList<String> videoKeys, ArrayList<String> videoNames) {
        mInflater = LayoutInflater.from(context);
        movieVideoKeys = videoKeys;
        movieVideoNames= videoNames;
    }

    @Override
    @NonNull
    public MovieVideoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.video_item, parent, false);
        return new MovieVideoAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieVideoAdapter.ViewHolder holder, int position) {
        if(movieVideoKeys != null &&movieVideoNames!= null) {
            holder.mVideoName.setVisibility(View.VISIBLE);
            holder.mVideoName.setText(movieVideoNames.get(position));
        }
        else{
            holder.mVideoName.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return movieVideoKeys == null ? 0 : movieVideoKeys.size();

    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mVideoName;
        String mVideoKey;

        ViewHolder(View itemView) {
            super(itemView);
            mVideoName= (TextView) itemView.findViewById(R.id.videoTitleTextView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }


    void setClickListener(MovieVideoAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }


    public void setMovieVideoData(ArrayList<String> videoNames,ArrayList<String> videoKeys) {
        movieVideoNames = videoNames;
        movieVideoKeys = videoKeys;
        notifyDataSetChanged();
    }

    public ArrayList<String> getMovieVideoKeys() {
        return movieVideoKeys;
    }

    public ArrayList<String> getMovieVideoNames() { return movieVideoNames; }
}
