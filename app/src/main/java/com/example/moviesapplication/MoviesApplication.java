package com.example.moviesapplication;

import android.app.Application;

import com.example.moviesapplication.data.AppDatabase;
import com.example.moviesapplication.utils.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MoviesApplication extends Application {

    private MoviesService service;
    private AppDatabase mDb;

    @Override
    public void onCreate() {
        super.onCreate();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.service = retrofit.create(MoviesService.class);
        mDb = AppDatabase.getInstance(getApplicationContext());
        mDb.reviewDao().nuke();
        mDb.movieDao().nuke();
        mDb.videoDao().nuke();

    }

    public MoviesService getService() {
        return service;
    }

    public AppDatabase getmDb() { return mDb; }
}
