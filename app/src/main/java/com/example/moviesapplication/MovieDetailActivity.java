package com.example.moviesapplication;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.moviesapplication.data.AppDatabase;
import com.example.moviesapplication.data.MovieEntry;
import com.example.moviesapplication.data.ReviewEntry;
import com.example.moviesapplication.data.VideoEntry;
import com.example.moviesapplication.utils.Constants;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MovieDetailActivity extends AppCompatActivity implements MovieVideoAdapter.ItemClickListener {


    private Button mFavoriteBtn;

    private String mTitle;
    private String mReleaseDate;
    private double mUserRating;
    private String mPlotSynopsis;
    private String mMoviePoster;
    private int mMovieId;

    private ArrayList<String> mVideoNames = new ArrayList<>();
    private ArrayList<String> mVideoKeys = new ArrayList<>();
    private ArrayList<String> mVideoIds = new ArrayList<>();

    private ArrayList<String> mReviewAuthors = new ArrayList<>();
    private ArrayList<String> mReviewContents = new ArrayList<>();
    private ArrayList<String> mReviewIds = new ArrayList<>();

    private MovieVideoAdapter mMovieVideoAdapter;

    private AppDatabase mDb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        TextView mTitleTextView = (TextView) findViewById(R.id.movie_title);
        TextView mReleaseDateTextView = (TextView) findViewById(R.id.movie_release);
        RatingBar mUserRatingBar = (RatingBar) findViewById(R.id.movie_rating);
        TextView mPlotSynopsisTextView = (TextView) findViewById(R.id.movie_plot);
        ImageView mMoviePosterImageView = (ImageView) findViewById(R.id.movie_poster);
        mFavoriteBtn = (Button) findViewById(R.id.buttonFavorites);

        RecyclerView mVideoRecyclerView = findViewById(R.id.movieVideosRecyclerView);
        RecyclerView mReviewRecyclerView = findViewById(R.id.movieReviewsRecyclerView);
        mVideoRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mReviewRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mDb = AppDatabase.getInstance(getApplicationContext());

        Intent intentDetails = getIntent();

        if (intentDetails != null) {
            if (intentDetails.hasExtra(Constants.TITLE_KEY) && intentDetails.hasExtra(Constants.IMAGE_KEY)
                    && intentDetails.hasExtra(Constants.PLOT_KEY) && intentDetails.hasExtra(Constants.RELEASE_KEY) &&
                    intentDetails.hasExtra(Constants.RATING_KEY) &&intentDetails.hasExtra(Constants.MOVIE_ID_KEY)) {

                mTitle = intentDetails.getStringExtra(Constants.TITLE_KEY);
                mTitleTextView.setText(mTitle);

                mReleaseDate = intentDetails.getStringExtra(Constants.RELEASE_KEY);
                mReleaseDateTextView.setText(mReleaseDate);

                mUserRating = intentDetails.getDoubleExtra(Constants.RATING_KEY, 0);
                mUserRatingBar.setRating((float) mUserRating / 2);

                mPlotSynopsis = intentDetails.getStringExtra(Constants.PLOT_KEY);
                mPlotSynopsisTextView.setText(mPlotSynopsis);

                mMoviePoster = intentDetails.getStringExtra(Constants.IMAGE_KEY);

                mMovieId = intentDetails.getIntExtra(Constants.MOVIE_ID_KEY,0);

                if(mDb.movieDao().loadMovieById(mMovieId)==null){
                    mFavoriteBtn.setText(getResources().getString(R.string.add_favorite));
                }
                else{
                    mFavoriteBtn.setText(getResources().getString(R.string.remove_favorite));
                }

                Glide
                        .with(this)
                        .load(Constants.IMAGE_BASE_PATH + mMoviePoster)
                        .centerCrop()
                        .placeholder(R.drawable.ic_launcher_background)
                        .into(mMoviePosterImageView);

                if (intentDetails.hasExtra(Constants.VIDEOS_KEYS) && intentDetails.hasExtra(Constants.VIDEOS_NAMES) && intentDetails.hasExtra(Constants.VIDEOS_IDS)) {
                    mVideoKeys = intentDetails.getStringArrayListExtra(Constants.VIDEOS_KEYS);
                    mVideoNames = intentDetails.getStringArrayListExtra(Constants.VIDEOS_NAMES);
                    mVideoIds= intentDetails.getStringArrayListExtra(Constants.VIDEOS_IDS);
                    mMovieVideoAdapter = new MovieVideoAdapter(this, mVideoNames, mVideoKeys);
                    mVideoRecyclerView.setAdapter(mMovieVideoAdapter);
                    mMovieVideoAdapter.setMovieVideoData(mVideoNames, mVideoKeys);
                    mMovieVideoAdapter.setClickListener(this);
                }


                if (intentDetails.hasExtra(Constants.REVIEWS_AUTHOR_KEY) && intentDetails.hasExtra(Constants.REVIEWS_CONTENT_KEY)) {
                    mReviewAuthors = intentDetails.getStringArrayListExtra(Constants.REVIEWS_AUTHOR_KEY);
                    mReviewContents = intentDetails.getStringArrayListExtra(Constants.REVIEWS_CONTENT_KEY);
                    mReviewIds= intentDetails.getStringArrayListExtra(Constants.REVIEWS_ID_KEY);
                    MovieReviewAdapter mMovieReviewAdapter = new MovieReviewAdapter(this, mReviewAuthors, mReviewContents);
                    mReviewRecyclerView.setAdapter(mMovieReviewAdapter);
                    mMovieReviewAdapter.setMovieReviewData(mReviewAuthors, mReviewContents);

                }

            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent startSettingsActivity = new Intent(this, SettingsActivity.class);
            startActivity(startSettingsActivity);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.YOUTUBE_BASE_VND + mMovieVideoAdapter.getMovieVideoKeys().get(position)));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(Constants.YOUTUBE_BASE_PATH + mMovieVideoAdapter.getMovieVideoKeys().get(position)));
        try {
            this.startActivity(webIntent);
        } catch (ActivityNotFoundException ex) {
            this.startActivity(appIntent);
        }
    }

    public void favoriteMovieClick(View view) {
        MovieEntry movieEntry= new MovieEntry(mMovieId,mPlotSynopsis,mTitle,mMoviePoster,mReleaseDate,mUserRating);
        MovieEntry test = mDb.movieDao().loadMovieById(mMovieId);
        if(mDb.movieDao().loadMovieById(mMovieId)==null){
            mDb.movieDao().insertMovie(movieEntry);
            test = mDb.movieDao().loadMovieById(mMovieId);
            for(int i = 0;i<mReviewIds.size();i++){
                String reviewId = mReviewIds.get(i);
                String author = mReviewAuthors.get(i);
                String content = mReviewContents.get(i);
                ReviewEntry reviewEntry = new ReviewEntry(reviewId,author,content,mMovieId);
                mDb.reviewDao().insertReview(reviewEntry);
                ArrayList<ReviewEntry> test2 = (ArrayList<ReviewEntry>)mDb.reviewDao().loadReviewsById(mMovieId);
            }
            for(int i = 0;i<mVideoIds.size();i++){
                VideoEntry videoEntry = new VideoEntry(mVideoIds.get(i),mMovieId,mVideoNames.get(i),mVideoKeys.get(i));
                mDb.videoDao().insertVideo(videoEntry);
            }
            mFavoriteBtn.setText(getResources().getString(R.string.remove_favorite));
        }
        else{
            mDb.movieDao().deleteMovie(movieEntry);
            mDb.reviewDao().deleteReviewById(mMovieId);
            mDb.videoDao().deleteVideoById(mMovieId);
            mFavoriteBtn.setText(getResources().getString(R.string.add_favorite));
        }

    }
}
