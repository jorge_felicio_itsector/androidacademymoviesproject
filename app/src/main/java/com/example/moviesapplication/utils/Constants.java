package com.example.moviesapplication.utils;

public class Constants {


    public static final String BASE_URL = "https://api.themoviedb.org/3/movie/";
    public static boolean PREFERENCES_UPDATE=false;
    public static final String POPULAR_SORT ="Popular";
    public static final String TOP_RATED_SORT ="Top Rated";
    public static final String FAVORITE_SORT ="Favorite";
    public static boolean REVIEW_CHECK=false;
    public static boolean VIDEO_CHECK= false;
    public static String TITLE_KEY ="title_key";
    public static String IMAGE_KEY ="image_key";
    public static String PLOT_KEY ="plot_key";
    public static String RATING_KEY ="rating_key";
    public static String RELEASE_KEY ="release_key";
    public static String MOVIE_ID_KEY ="id_key";
    public static String REVIEWS_AUTHOR_KEY ="reviews_author";
    public static String REVIEWS_CONTENT_KEY ="reviews_content";
    public static String REVIEWS_ID_KEY ="reviews_ids";
    public static String VIDEOS_KEYS ="videos_keys";
    public static String VIDEOS_NAMES ="videos_names";
    public static String VIDEOS_IDS ="videos_ids";
    public static String IMAGE_BASE_PATH="https://image.tmdb.org/t/p/w500/";
    public static String YOUTUBE_BASE_VND="vnd.youtube:";
    public static String YOUTUBE_BASE_PATH="http://www.youtube.com/watch?v=";





}
