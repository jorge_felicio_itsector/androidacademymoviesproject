package com.example.moviesapplication.utils;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.preference.PreferenceManager;

import com.example.moviesapplication.R;

public class PreferenceChecker {
    public static String getSorting(Context context) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        String keyForSorting = context.getString(R.string.pref_sorting_key);
        String defaultSorting = context.getString(R.string.pref_popular_label);
        String preferredSorting = prefs.getString(keyForSorting, defaultSorting);
        if (Constants.POPULAR_SORT.equalsIgnoreCase(preferredSorting)) {
            return Constants.POPULAR_SORT;
        } else if (Constants.TOP_RATED_SORT.equalsIgnoreCase(preferredSorting)){
            return Constants.TOP_RATED_SORT;
        } else{
            return Constants.FAVORITE_SORT;
        }
    }
}
