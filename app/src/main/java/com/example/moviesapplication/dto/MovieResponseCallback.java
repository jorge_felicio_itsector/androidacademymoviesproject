package com.example.moviesapplication.dto;

import com.example.moviesapplication.data.MovieEntry;
import com.example.moviesapplication.data.ReviewEntry;
import com.example.moviesapplication.data.VideoEntry;
import android.content.Intent;


import java.util.ArrayList;

public interface MovieResponseCallback {
    public void onFinish(ArrayList<MovieEntry> movies);
}
