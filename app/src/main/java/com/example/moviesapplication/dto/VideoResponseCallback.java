package com.example.moviesapplication.dto;

import com.example.moviesapplication.data.VideoEntry;

import java.util.ArrayList;
import android.content.Intent;


public interface VideoResponseCallback {
    public void onFinish(Intent detailIntent);
}
