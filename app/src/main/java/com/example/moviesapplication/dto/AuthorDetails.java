package com.example.moviesapplication.dto;

import com.google.gson.annotations.SerializedName;

public class AuthorDetails{

	@SerializedName("avatar_path")
	private Object avatarPath;

	@SerializedName("name")
	private String name;

	@SerializedName("rating")
	private double rating;

	@SerializedName("username")
	private String username;

	public Object getAvatarPath(){
		return avatarPath;
	}

	public String getName(){
		return name;
	}

	public double getRating(){
		return rating;
	}

	public String getUsername(){
		return username;
	}
}