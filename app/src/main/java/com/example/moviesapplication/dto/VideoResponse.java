package com.example.moviesapplication.dto;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class VideoResponse {

	@SerializedName("id")
	private int id;

	@SerializedName("results")
	private List<VideoResponseItem> results;

	public int getId(){
		return id;
	}

	public List<VideoResponseItem> getResults(){
		return results;
	}
}