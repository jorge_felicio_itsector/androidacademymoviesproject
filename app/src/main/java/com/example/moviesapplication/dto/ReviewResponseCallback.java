package com.example.moviesapplication.dto;

import com.example.moviesapplication.data.ReviewEntry;

import java.util.ArrayList;
import android.content.Intent;


public interface ReviewResponseCallback {
    public void onFinish(int id,Intent detailIntent);
}
