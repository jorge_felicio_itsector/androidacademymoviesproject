package com.example.moviesapplication.dto;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ReviewResponse {

	@SerializedName("id")
	private int id;

	@SerializedName("page")
	private int page;

	@SerializedName("total_pages")
	private int totalPages;

	@SerializedName("results")
	private List<ReviewResponseItem> results;

	@SerializedName("total_results")
	private int totalResults;

	public int getId(){
		return id;
	}

	public int getPage(){
		return page;
	}

	public int getTotalPages(){
		return totalPages;
	}

	public List<ReviewResponseItem> getResults(){
		return results;
	}

	public int getTotalResults(){
		return totalResults;
	}
}