package com.example.moviesapplication;

import com.example.moviesapplication.dto.MovieResponse;
import com.example.moviesapplication.dto.ReviewResponse;
import com.example.moviesapplication.dto.VideoResponse;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MoviesService {

    @GET("popular")
    Call<MovieResponse> fetchPopular(@Query("api_key") String api_key);

    @GET("top_rated")
    Call<MovieResponse> fetchTopRated(@Query("api_key") String api_key);

    @GET("{id}/reviews")
    Call<ReviewResponse> fetchReviewsById(@Path("id") String var,@Query("api_key") String api_key);

    @GET("{id}/videos")
    Call<VideoResponse> fetchVideosById(@Path("id") String var,@Query("api_key") String api_key);
}
