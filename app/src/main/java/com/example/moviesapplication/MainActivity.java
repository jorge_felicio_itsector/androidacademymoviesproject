package com.example.moviesapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.moviesapplication.data.AppDatabase;
import com.example.moviesapplication.data.MovieEntry;
import com.example.moviesapplication.data.ReviewEntry;
import com.example.moviesapplication.data.VideoEntry;
import com.example.moviesapplication.dto.MovieResponse;
import com.example.moviesapplication.dto.MovieResponseCallback;
import com.example.moviesapplication.dto.MovieResponseItem;
import com.example.moviesapplication.dto.ReviewResponse;
import com.example.moviesapplication.dto.ReviewResponseCallback;
import com.example.moviesapplication.dto.ReviewResponseItem;
import com.example.moviesapplication.dto.VideoResponse;
import com.example.moviesapplication.dto.VideoResponseCallback;
import com.example.moviesapplication.dto.VideoResponseItem;
import com.example.moviesapplication.utils.Constants;
import com.example.moviesapplication.utils.PreferenceChecker;
import com.example.moviesapplication.utils.SecretConstants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener, MovieAdapter.ItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    public Context context;
    private ArrayList<MovieEntry> movieList;
    private MovieAdapter movieAdapter;
    private RecyclerView mRecyclerView;

    private MoviesService service;

    private MovieResponseCallback movieCallback;

    private VideoResponseCallback videoCallback;

    private ReviewResponseCallback reviewCallback;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private ProgressBar mProgressBar;

    private AppDatabase mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = findViewById(R.id.movieRecyclerView);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, getResources().getInteger(R.integer.column_number)));
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        context = this;
        mProgressBar = findViewById(R.id.loading);
        movieCallback = new MovieResponseCallback() {
            @Override
            public void onFinish(ArrayList<MovieEntry> movies) {
                if (movieAdapter == null) {
                    movieAdapter = new MovieAdapter(context, movieList);
                    mRecyclerView.setAdapter(movieAdapter);
                }
                movieAdapter.setMovieData(movieList);
                setOnClickListener();
                mSwipeRefreshLayout.setRefreshing(false);
                mProgressBar.setVisibility(View.GONE);
            }

        };
        videoCallback = new VideoResponseCallback() {
            @Override
            public void onFinish(Intent detailIntent) {
                startActivity(detailIntent);
            }

        };
        reviewCallback = new ReviewResponseCallback() {
            @Override
            public void onFinish(int id, Intent detailIntent) {
                loadVideos(id, videoCallback, detailIntent);
            }

        };

        mSwipeRefreshLayout = findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                mSwipeRefreshLayout.setRefreshing(true);
                loadData(movieCallback);

            }
        });

    }

    private void setOnClickListener() {
        movieAdapter.setClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (Constants.PREFERENCES_UPDATE) {
            Constants.PREFERENCES_UPDATE = false;
            loadData(movieCallback);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.moviemenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent startSettingsActivity = new Intent(this, SettingsActivity.class);
            startActivity(startSettingsActivity);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Constants.PREFERENCES_UPDATE = true;

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Constants.PREFERENCES_UPDATE) {
            Constants.PREFERENCES_UPDATE = false;
            loadData(movieCallback);
        }
    }

    public void loadData(final MovieResponseCallback callback) {
        mProgressBar.setVisibility(View.VISIBLE);
        service = ((MoviesApplication) getApplication()).getService();
        mSwipeRefreshLayout.setRefreshing(true);
        mDb = ((MoviesApplication) getApplication()).getmDb();
        String sorting = PreferenceChecker.getSorting(context);
        movieList = new ArrayList<>();
        if (sorting.equalsIgnoreCase(Constants.POPULAR_SORT)) {
            service.fetchPopular(SecretConstants.API_KEY).enqueue(new Callback<MovieResponse>() {
                @Override
                public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                    ArrayList<MovieResponseItem> results = (ArrayList<MovieResponseItem>) response.body().getResults();
                    for (int i = 0; i < results.size(); i++) {
                        int id = results.get(i).getId();
                        String overview = results.get(i).getOverview();
                        String originalTitle = results.get(i).getOriginalTitle();
                        String backdropPath = results.get(i).getBackdropPath();
                        String releaseDate = results.get(i).getReleaseDate();
                        double voteAverage = results.get(i).getVoteAverage();
                        MovieEntry entry = new MovieEntry(id, overview, originalTitle, backdropPath, releaseDate, voteAverage);
                        movieList.add(entry);
                    }
                    callback.onFinish(movieList);

                }

                @Override
                public void onFailure(@NotNull Call<MovieResponse> call, Throwable t) {
                    Toast.makeText(context, getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                    mSwipeRefreshLayout.setRefreshing(false);
                    mProgressBar.setVisibility(View.GONE);

                }


            });
        } else if (sorting.equalsIgnoreCase(Constants.TOP_RATED_SORT)) {
            service.fetchTopRated(SecretConstants.API_KEY).enqueue(new Callback<MovieResponse>() {
                @Override
                public void onResponse(@NotNull Call<MovieResponse> call, @NotNull Response<MovieResponse> response) {
                    ArrayList<MovieResponseItem> results = (ArrayList<MovieResponseItem>) response.body().getResults();
                    for (int i = 0; i < results.size(); i++) {
                        int id = results.get(i).getId();
                        String overview = results.get(i).getOverview();
                        String originalTitle = results.get(i).getOriginalTitle();
                        String backdropPath = results.get(i).getBackdropPath();
                        String releaseDate = results.get(i).getReleaseDate();
                        double voteAverage = results.get(i).getVoteAverage();
                        MovieEntry entry = new MovieEntry(id, overview, originalTitle, backdropPath, releaseDate, voteAverage);
                        movieList.add(entry);
                    }
                    callback.onFinish(movieList);

                }

                @Override
                public void onFailure(Call<MovieResponse> call, Throwable t) {
                    Toast.makeText(context, getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                    mSwipeRefreshLayout.setRefreshing(false);
                    mProgressBar.setVisibility(View.GONE);

                }


            });
        } else if (sorting.equalsIgnoreCase(Constants.FAVORITE_SORT)) {
            ArrayList<MovieEntry> test = (ArrayList<MovieEntry>) mDb.movieDao().loadAllMovies();
            if (mDb.movieDao().loadAllMovies().size() != 0) {
                movieList = (ArrayList<MovieEntry>) mDb.movieDao().loadAllMovies();
                if (movieAdapter == null) {
                    movieAdapter = new MovieAdapter(context, movieList);
                    mRecyclerView.setAdapter(movieAdapter);
                }
                movieAdapter.setMovieData(movieList);
                setOnClickListener();
                mSwipeRefreshLayout.setRefreshing(false);
                mProgressBar.setVisibility(View.GONE);
            }
        }


    }


    @Override
    public void onRefresh() {
        if (movieAdapter != null) {
            movieAdapter.setMovieData(null);
        }
        loadData(movieCallback);
    }


    @Override
    public void onItemClick(View view, int position) {
        Context context = this;
        Class<MovieDetailActivity> destinationClass = MovieDetailActivity.class;
        Intent movieDetailActivityIntent = new Intent(context, destinationClass);
        MovieEntry item = movieAdapter.getMovieList().get(position);
        String title = item.getOriginalTitle();
        String image = item.getBackdropPath();
        String plotSynopsis = item.getOverview();
        double userRating = item.getRating();
        String releaseDate = item.getReleaseDate();
        int movieId = item.getMovieId();
        movieDetailActivityIntent.putExtra(Constants.TITLE_KEY, title);
        movieDetailActivityIntent.putExtra(Constants.IMAGE_KEY, image);
        movieDetailActivityIntent.putExtra(Constants.PLOT_KEY, plotSynopsis);
        movieDetailActivityIntent.putExtra(Constants.RATING_KEY, userRating);
        movieDetailActivityIntent.putExtra(Constants.RELEASE_KEY, releaseDate);
        movieDetailActivityIntent.putExtra(Constants.MOVIE_ID_KEY, movieId);
        loadReviews(item.getMovieId(), reviewCallback, movieDetailActivityIntent);
    }

    private void loadReviews(int id, ReviewResponseCallback callback, Intent detailIntent) {
        service = ((MoviesApplication) getApplication()).getService();
        mDb = ((MoviesApplication) getApplication()).getmDb();
        if (mDb.reviewDao().loadReviewsById(id).size() != 0) {
            ArrayList<String> author = new ArrayList<>();
            ArrayList<String> content = new ArrayList<>();
            ArrayList<String> reviewIds = new ArrayList<>();
            ArrayList<ReviewEntry> reviews = (ArrayList<ReviewEntry>) mDb.reviewDao().loadReviewsById(id);
            for (int i = 0; i < reviews.size(); i++) {
                author.add(reviews.get(i).getAuthor());
                content.add(reviews.get(i).getContent());
                reviewIds.add(reviews.get(i).getReviewId());
            }

            detailIntent.putExtra(Constants.REVIEWS_AUTHOR_KEY, author);
            detailIntent.putExtra(Constants.REVIEWS_CONTENT_KEY, content);
            detailIntent.putExtra(Constants.REVIEWS_ID_KEY, reviewIds);
            loadVideos(id, videoCallback, detailIntent);
        } else {
            service.fetchReviewsById(String.valueOf(id), SecretConstants.API_KEY).enqueue(new Callback<ReviewResponse>() {
                @Override
                public void onResponse(@NotNull Call<ReviewResponse> call, @NotNull Response<ReviewResponse> response) {
                    ArrayList<String> author = new ArrayList<>();
                    ArrayList<String> content = new ArrayList<>();
                    ArrayList<String> reviewIds = new ArrayList<>();
                    ArrayList<ReviewResponseItem> results = (ArrayList<ReviewResponseItem>) response.body().getResults();
                    for (int i = 0; i < results.size(); i++) {
                        author.add(results.get(i).getAuthor());
                        content.add(results.get(i).getContent());
                        reviewIds.add(results.get(i).getId());
                    }

                    detailIntent.putExtra(Constants.REVIEWS_AUTHOR_KEY, author);
                    detailIntent.putExtra(Constants.REVIEWS_CONTENT_KEY, content);
                    detailIntent.putExtra(Constants.REVIEWS_ID_KEY, reviewIds);
                    callback.onFinish(id, detailIntent);
                }

                @Override
                public void onFailure(@NotNull Call<ReviewResponse> call, @NotNull Throwable t) {
                }


            });
        }
    }

    private void loadVideos(int id, VideoResponseCallback callback, Intent detailIntent) {
        service = ((MoviesApplication) getApplication()).getService();
        mDb = mDb = ((MoviesApplication) getApplication()).getmDb();
        if (mDb.videoDao().loadVideosById(id).size() != 0) {
            ArrayList<VideoEntry> videos = (ArrayList<VideoEntry>) mDb.videoDao().loadVideosById(id);
            ArrayList<String> keys = new ArrayList<>();
            ArrayList<String> names = new ArrayList<>();
            ArrayList<String> videoIds = new ArrayList<>();
            for (int i = 0; i < videos.size(); i++) {
                keys.add(videos.get(i).getKey());
                names.add(videos.get(i).getName());
                videoIds.add(videos.get(i).getVideoId());
            }
            detailIntent.putExtra(Constants.VIDEOS_KEYS, keys);
            detailIntent.putExtra(Constants.VIDEOS_NAMES, names);
            detailIntent.putExtra(Constants.VIDEOS_IDS, videoIds);
            startActivity(detailIntent);

        } else {
            service.fetchVideosById(String.valueOf(id), SecretConstants.API_KEY).enqueue(new Callback<VideoResponse>() {
                @Override
                public void onResponse(@NotNull Call<VideoResponse> call, @NotNull Response<VideoResponse> response) {
                    ArrayList<String> keys = new ArrayList<>();
                    ArrayList<String> names = new ArrayList<>();
                    ArrayList<String> videoIds = new ArrayList<>();
                    ArrayList<VideoResponseItem> results = (ArrayList<VideoResponseItem>) response.body().getResults();
                    for (int i = 0; i < results.size(); i++) {
                        keys.add(results.get(i).getKey());
                        names.add(results.get(i).getName());
                        videoIds.add(results.get(i).getId());
                    }
                    detailIntent.putExtra(Constants.VIDEOS_KEYS, keys);
                    detailIntent.putExtra(Constants.VIDEOS_NAMES, names);
                    detailIntent.putExtra(Constants.VIDEOS_IDS, videoIds);
                    callback.onFinish(detailIntent);

                }

                @Override
                public void onFailure(@NotNull Call<VideoResponse> call, @NotNull Throwable t) {
                }


            });
        }
    }
}

