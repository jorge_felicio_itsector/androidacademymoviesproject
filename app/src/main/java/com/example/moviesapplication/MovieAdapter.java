package com.example.moviesapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.moviesapplication.data.MovieEntry;
import com.example.moviesapplication.dto.MovieResponseItem;
import com.example.moviesapplication.utils.Constants;

import java.util.ArrayList;

public class MovieAdapter  extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

    private ArrayList<MovieEntry> movieList = new ArrayList<MovieEntry>();
    private final LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private final Context mContext;


    public MovieAdapter(Context context,  ArrayList<MovieEntry> objects) {
        mInflater = LayoutInflater.from(context);
        mContext=context;
        movieList = objects;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.gridview_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(movieList != null ) {
            holder.myImageView.setVisibility(View.VISIBLE);
            Glide
                    .with(mContext)
                    .load(Constants.IMAGE_BASE_PATH + getMovieList().get(position).getBackdropPath())
                    .centerCrop()
                    .placeholder(R.drawable.ic_launcher_background)
                    .into(holder.myImageView);
        }
        else{
            holder.myImageView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return movieList == null ? 0 : movieList.size();

    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView myImageView;

        ViewHolder(View itemView) {
            super(itemView);
            myImageView = (ImageView) itemView.findViewById(R.id.movieImageView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    MovieEntry getItem(int id) {
        return movieList.get(id);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }


    public void setMovieData(ArrayList<MovieEntry> movieData) {
        movieList = movieData;
        notifyDataSetChanged();
    }

    public ArrayList<MovieEntry> getMovieList() {
        return movieList;
    }
}
