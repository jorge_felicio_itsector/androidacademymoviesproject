package com.example.moviesapplication.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import androidx.room.OnConflictStrategy;

import java.util.List;

@Dao
public interface MovieDetailedDao {

    @Query("SELECT * FROM movie ")
    List<MovieEntry> loadAllMovies();

    @Insert
    long insertMovie(MovieEntry movieEntry);

    @Delete
    void deleteMovie(MovieEntry movieEntry);

    @Query("SELECT * FROM movie WHERE movieId = :id")
    MovieEntry loadMovieById(int id);

    @Query("DELETE FROM movie")
    void nuke();

}
