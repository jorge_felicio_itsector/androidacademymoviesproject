package com.example.moviesapplication.data;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.ColumnInfo;

import com.example.moviesapplication.dto.AuthorDetails;

import java.util.List;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "review")
public class ReviewEntry {

    @NonNull
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "reviewId")
    private String reviewId;
    @ColumnInfo(name = "author")
    private String author;
    @ColumnInfo(name = "content")
    private String content;
    @ColumnInfo(name = "movieId")
    private int movieId;

    public ReviewEntry(@NonNull String reviewId, String author, String content, int movieId) {
        this.reviewId = reviewId;
        this.author = author;
        this.content = content;
        this.movieId = movieId;
    }

    @NonNull
    public String getReviewId() {
        return reviewId;
    }

    public void setReviewId(@NonNull String reviewId) {
        this.reviewId = reviewId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }
}
