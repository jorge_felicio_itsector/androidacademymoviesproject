package com.example.moviesapplication.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.OnConflictStrategy;

import java.util.List;

@Dao
public interface VideoDao {
    @Insert
    long insertVideo(VideoEntry videoEntry);

    @Delete
    void deleteVideo(VideoEntry videoEntry);

    @Query("SELECT * FROM video WHERE movieId = :id")
    List<VideoEntry> loadVideosById(int id);

    @Query("DELETE FROM video WHERE movieId = :id")
    void deleteVideoById(int id);

    @Query("DELETE FROM video")
    void nuke();
}
