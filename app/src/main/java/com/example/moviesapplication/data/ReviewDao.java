package com.example.moviesapplication.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.OnConflictStrategy;

import java.util.List;

@Dao
public interface ReviewDao {

    @Insert
    long insertReview(ReviewEntry reviewEntry);

    @Delete
    void deleteReview(ReviewEntry reviewEntry);

    @Query("SELECT * FROM review WHERE movieId = :id")
    List<ReviewEntry> loadReviewsById(int id);

    @Query("DELETE FROM review WHERE movieId = :id")
    void deleteReviewById(int id);

    @Query("DELETE FROM review")
    void nuke();
}
