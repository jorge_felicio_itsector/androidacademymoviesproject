package com.example.moviesapplication.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.ColumnInfo;


import java.util.Date;
import java.util.List;

@Entity(tableName = "movie")
public class MovieEntry {

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "movieId")
    private int movieId;
    @ColumnInfo(name = "overview")
    private String overview;
    @ColumnInfo(name = "original_title")
    private String originalTitle;
    @ColumnInfo(name = "backdrop_path")
    private String backdropPath;
    @ColumnInfo(name = "release_date")
    private String releaseDate;
    @ColumnInfo(name = "rating")
    private double rating;

    public MovieEntry(int movieId, String overview, String originalTitle, String backdropPath, String releaseDate, double rating) {
        this.movieId = movieId;
        this.overview = overview;
        this.originalTitle = originalTitle;
        this.backdropPath = backdropPath;
        this.releaseDate = releaseDate;
        this.rating = rating;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
