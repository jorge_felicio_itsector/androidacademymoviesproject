package com.example.moviesapplication.data;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.ColumnInfo;

import static androidx.room.ForeignKey.CASCADE;


@Entity(tableName = "video")
public class VideoEntry {

    @NonNull
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "videoId")
    private String videoId;
    @ColumnInfo(name = "movieId")
    private int movieId;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "key")
    private String key;

    public VideoEntry(@NonNull String videoId, int movieId, String name, String key) {
        this.videoId = videoId;
        this.movieId = movieId;
        this.name = name;
        this.key = key;
    }

    @NonNull
    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(@NonNull String videoId) {
        this.videoId = videoId;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
