package com.example.moviesapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MovieReviewAdapter extends RecyclerView.Adapter<MovieReviewAdapter.ViewHolder> {
    ArrayList<String> movieReviewAuthors;
    ArrayList<String> movieReviewContents;
    LayoutInflater mInflater;
    Context context;

    public MovieReviewAdapter(Context context,  ArrayList<String> reviewAuthors, ArrayList<String> reviewContents) {
        mInflater = LayoutInflater.from(context);
        context=context;
        movieReviewAuthors = reviewAuthors;
        movieReviewContents= reviewContents;
    }

    @Override
    @NonNull
    public MovieReviewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.review_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieReviewAdapter.ViewHolder holder, int position) {
        if(movieReviewAuthors != null &&movieReviewContents!= null) {
            holder.mReviewAuthor.setVisibility(View.VISIBLE);
            holder.mReviewContent.setVisibility(View.VISIBLE);

            holder.mReviewAuthor.setText(movieReviewAuthors.get(position));
            holder.mReviewContent.setText(movieReviewContents.get(position));
        }
        else{
            holder.mReviewAuthor.setVisibility(View.GONE);
            holder.mReviewContent.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return movieReviewAuthors == null ? 0 : movieReviewAuthors.size();

    }



    public static class ViewHolder extends RecyclerView.ViewHolder  {
        TextView mReviewAuthor;
        TextView mReviewContent;

        ViewHolder(View itemView) {
            super(itemView);
            mReviewAuthor= (TextView) itemView.findViewById(R.id.reviewAuthorTextView);
            mReviewContent= (TextView) itemView.findViewById(R.id.reviewContentTextView);
        }

    }



    public void setMovieReviewData(ArrayList<String> reviewAuthors,ArrayList<String> reviewContents) {
        movieReviewAuthors= reviewAuthors;
        movieReviewContents = reviewContents;
        notifyDataSetChanged();
    }

    public ArrayList<String> getMovieReviewAuthors() { return movieReviewAuthors; }

    public ArrayList<String> getMovieReviewContents() { return movieReviewContents; }
}
